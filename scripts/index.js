'use strict'


window.onload = function () {
    fetch("./us.json")
        .then((data) => data.json())
        .then((array) =>
            loadCitySelect(array)
        )

}

function loadCitySelect(cityList) {
    const citySelect = document.getElementById("citySelect");

    for (let i = 0; i < cityList.length; i++) {
        let theOption = new Option(cityList[i].name, cityList[i].lat + "," + cityList[i].lng);
        citySelect.appendChild(theOption);
    }
    citySelect.onchange = onChangeCity;
}
function onChangeCity() {
    let cityIndex = document.getElementById("citySelect").selectedIndex;

    fetch("https://api.weather.gov/points/" + citySelect[cityIndex].value)
        .then(response => response.json())
        .then(data => parseObject(data))
}

function parseObject(data) {
    fetch(data.properties.forecast)
        .then(response => response.json())
        .then(data => loadTable(data.properties.periods))
}

function loadTable(array) {
    const forcastTblBody = document.getElementById("forcastTblBody");

    for (let i = 0; i < array.length; i++) {
        let row = forcastTblBody.insertRow(-1);
        let cell1 = row.insertCell(0);
        let cell2 = row.insertCell(1);
        let cell3 = row.insertCell(2);
        let cell4 = row.insertCell(3);

        cell1.innerHTML = array[i].name;
        cell2.innerHTML = `Temperature ${array[i].temperature} ${array[i].temperatureUnit}`;
        cell3.innerHTML = `Winds ${array[i].windDirection} ${array[i].windSpeed}`;
        cell4.innerHTML = array[i].shortForecast;
    }
}